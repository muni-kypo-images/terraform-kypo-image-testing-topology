output "pool_url" {
  value       = module.sandbox.pool_url
  description = "`pool_url` output from `sandbox-ci` module."
}
